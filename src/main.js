import { createApp } from "vue";
import App from "./App.vue";
import router from "./utils/router.js";
import store from "./utils/store";

createApp(App)
.use(store)
.use(router)
.mount("#app");
