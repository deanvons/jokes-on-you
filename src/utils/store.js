import { createStore } from "vuex";

const apiURL = "https://noroff-assignment-users-api.herokuapp.com";
const apiKey = "api_key";

const store = createStore({
  state: {
    jokeURL: "",
    user: [],
  },
  mutations: {
    setJokeURL: (state, payload) => {
      state.jokeURL = payload;
    },
    setUser: (state, payload) => {
      state.user = payload;
    },
  },
  actions: {
    async attemptLogin({commit,dispatch},name) {
        console.log(name)
      return fetch(`${apiURL}/trivia?username=${name}`)
        .then((response) => response.json())
        .then((user) => {
          if (user.length == 0) {
          dispatch('register',name)  ;
          } else {
            commit('setUser', user);
          }
        })
        .catch((error) => {});
    },

    async register({commit},name) {
      //fixed
      return fetch(`${apiURL}/trivia`, {
        method: "POST",
        headers: {
          "X-API-Key": apiKey,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username: name,
          highScore: 0,
        }),
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error("Could not create new user");
          }
          return response.json();
        })
        .then((newUser) => {
            commit('setUser', newUser);
        })
        .catch((error) => {});
    },
  },
});

export default store;
