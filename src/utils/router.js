
import Start from "../components/view/StartView.vue";
import Jokes from "../components/view/JokesView.vue";
import Results from "../components/view/ResultsView.vue";
import { createRouter, createWebHistory } from "vue-router";
const routes = [
    {
      path: "/",
      component: Start,
    },
    {
      path: "/jokes",
      component: Jokes,
    }
    ,
    {
      path: "/results",
      component: Results,
    }
  ];
  
  export default createRouter({
    history: createWebHistory(),
    routes,
  });